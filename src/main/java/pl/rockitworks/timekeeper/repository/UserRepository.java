package pl.rockitworks.timekeeper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.User;

import java.util.Set;

/**
 * @author Bartosz Flis
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findOneByName(String name);

    Set<User> findByNameLike(String some);

    User findByEmail(String email);


}
