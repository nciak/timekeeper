package pl.rockitworks.timekeeper.repository;
//Kuba
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Note;

import java.util.List;
@Repository
public interface NoteRepository extends JpaRepository <Note, Long>
{
    @Query("select n from Note n where n.comment like %?1%")
    List<Note> selectPassingNotes (String pattern);
}
