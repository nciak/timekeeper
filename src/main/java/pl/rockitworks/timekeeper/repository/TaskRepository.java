package pl.rockitworks.timekeeper.repository;

//Kuba

import org.springframework.data.domain.Example;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository <Task, Long>
{
    List<Task> findAllByName (String name);

    @Query("select t from Task t where t.name like ?1%")
    List <Task> findAlike (String alike);

    void deleteByName(String name);
}
