package pl.rockitworks.timekeeper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Project;

import java.util.List;
import java.util.Set;

/**
 * @author Norbert Ciak
 */

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findByName(String name);

    Set<Project> findByIsActive(Boolean isActive);

}
