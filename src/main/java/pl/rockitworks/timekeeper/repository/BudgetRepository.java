package pl.rockitworks.timekeeper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Budget;
import pl.rockitworks.timekeeper.entity.Project;

import java.math.BigDecimal;

/**
 * @author Norbert Ciak
 */

@Repository
public interface BudgetRepository extends JpaRepository<Budget, Long> {

    Budget findByRevenue(BigDecimal revenue);
}
