package pl.rockitworks.timekeeper.repository;

//Author: Sławomir Rogala

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Skill;

import java.util.List;


@Repository
public interface SkillRepository extends JpaRepository<Skill, Long> {

    Skill findOneByName(String name);


}