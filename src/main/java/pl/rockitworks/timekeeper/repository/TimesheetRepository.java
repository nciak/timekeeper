package pl.rockitworks.timekeeper.repository;

//Author: Sławomir Rogala

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Timesheet;


@Repository

public interface TimesheetRepository extends JpaRepository<Timesheet, Long> {

    Timesheet findByValue(int value);


}

