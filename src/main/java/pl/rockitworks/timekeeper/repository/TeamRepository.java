package pl.rockitworks.timekeeper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Team;

import java.util.Set;

/**
 * @author Kamil Ulatowski
 */
@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Team findByName(String name);

    Set<Team> findByNameLike(String pattern);
}