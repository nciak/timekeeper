package pl.rockitworks.timekeeper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.rockitworks.timekeeper.entity.Program;

import java.util.List;

@Repository
public interface ProgramRepository extends JpaRepository<Program, Long> {

    Program findOneByName(String name);

    @Query("select p from Project p where p.name like ?1%")
    List<Program> findBySome(String some);
}
