package pl.rockitworks.timekeeper.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.rockitworks.timekeeper.dto.ProgramDto;
import pl.rockitworks.timekeeper.dto.ProjectDto;
import pl.rockitworks.timekeeper.entity.Program;
import pl.rockitworks.timekeeper.entity.Project;
import pl.rockitworks.timekeeper.repository.ProgramRepository;

import java.util.HashSet;
import java.util.Set;

/**
 * Author: Jakub Grodziński
 */

@Component
public class ProgramConverter
{

    private final ProgramRepository programRepository;


    private final ProjectConverter projectConverter;


    private final UserConverter userConverter;

    @Autowired
    public ProgramConverter(ProgramRepository programRepository, ProjectConverter projectConverter, UserConverter userConverter) {
        this.programRepository = programRepository;
        this.projectConverter = projectConverter;
        this.userConverter = userConverter;
    }

    public ProgramDto convertEntityToDto (Program program){
        ProgramDto programDto = new ProgramDto();
        programDto.setId(program.getId());
        programDto.setName(program.getName());
        Set<ProjectDto> projectDtos = new HashSet<>();
        programDto.setCreatedBy(userConverter.convertEntityToDto(program.getCreatedBy()));
        for (Project project: program.getProjects()) {
            ProjectDto projectDto = projectConverter.convertEntityToDto(project);
            projectDtos.add(projectDto);
        }
        programDto.setProjectDtos(projectDtos);
        return programDto;
    }

    public Program convertDtoToEntity (ProgramDto programDto)
    {
        Program programInDb = programRepository.getOne(programDto.getId());
        programInDb.setName(programDto.getName());
        Set<Project> projectSet = new HashSet<>();
        programInDb.setCreatedBy(userConverter.convertDtoToEntity(programDto.getCreatedBy()));
        for(ProjectDto projectDto : programDto.getProjectDtos())
        {
            Project project = projectConverter.convertDtoToEntity(projectDto);
            projectSet.add(project);
        }

        programInDb.setProjects(projectSet);


        return programInDb;
    }

    public Set<ProgramDto> convertSetFromEntityToDto (Set<Program> programEntity) {
        Set<ProgramDto> programDtos = new HashSet<>();
        for (Program program : programEntity) {

            ProgramDto programDto = convertEntityToDto(program);
            programDtos.add(programDto);
        }

        return programDtos;
    }

    public Set<Program> convertSetFromDtoToEntity (Set<ProgramDto> programDtos){
        Set<Program> programEntities = new HashSet<>();
        for (ProgramDto programDto: programDtos) {

            Program programEntity = convertDtoToEntity(programDto);
            programEntities.add(programEntity);
        }

        return programEntities;
    }
}
