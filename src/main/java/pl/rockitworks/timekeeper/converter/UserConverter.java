package pl.rockitworks.timekeeper.converter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.rockitworks.timekeeper.dto.ProgramDto;
import pl.rockitworks.timekeeper.dto.UserDto;
import pl.rockitworks.timekeeper.entity.Program;
import pl.rockitworks.timekeeper.entity.User;
import pl.rockitworks.timekeeper.repository.UserRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Bartosz Flis
 */
@Component
public class UserConverter {

    private final UserRepository userRepository;


    @Autowired
    public UserConverter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public UserDto convertEntityToDto (User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setName(user.getName());
        return userDto;

    }

    public User convertDtoToEntity(UserDto userDto) {
        User userInDb = userRepository.getOne(userDto.getId());
        userInDb.setId(userDto.getId());
        userInDb.setEmail(userDto.getEmail());
        userInDb.setName(userDto.getName());
        return userInDb;



    }


    public Set<UserDto> convertSetEntityToDto(Set<User> users) {
        Set<UserDto> userDtos = new HashSet<>();
        for (User user : users) {
            UserDto userDto = convertEntityToDto(user);
            userDtos.add(userDto);
        }
        return  userDtos;
    }

    public Set<User> convertSetDtoToEntity (Set<UserDto> userDtos)
    {
        Set<User> users = new HashSet<>();
        for (UserDto userDto : userDtos) {
            User user = convertDtoToEntity(userDto);
            users.add(user);
        }
        return users;
    }





}
