package pl.rockitworks.timekeeper.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.rockitworks.timekeeper.dto.TeamDto;
import pl.rockitworks.timekeeper.entity.Team;
import pl.rockitworks.timekeeper.repository.TeamRepository;

import java.util.HashSet;
import java.util.Set;

// @Kamil Ulatowski
@Component
public class TeamConverter {

    @Autowired
    TeamRepository teamRepository;
    UserConverter userConverter;
    ProjectConverter projectConverter;

    public Team convertDtoToEntity(TeamDto teamDto) {

        Team team = teamRepository.getOne(teamDto.getId());
        team.setId(teamDto.getId());
        team.setName(teamDto.getName());
        team.setLeader(userConverter.convertDtoToEntity(teamDto.getLeader()));
        team.setProjects(projectConverter.convertSetDtoToEntity(teamDto.getProjects()));
        team.setMembers(userConverter.convertSetDtoToEntity(teamDto.getMembers()));

        return team;
    }

    public TeamDto convertEntityToDto(Team team) {

        TeamDto teamDto = new TeamDto();
        teamDto.setId(team.getId());
        teamDto.setName(team.getName());
        teamDto.setLeader(userConverter.convertEntityToDto(team.getLeader()));
        teamDto.setProjects(projectConverter.convertSetEntityToDto(team.getProjects()));
        teamDto.setMembers(userConverter.convertSetEntityToDto(team.getMembers()));

        return teamDto;
    }

    public Set<Team> convertSetDtoToEntity(Set<TeamDto> teamDtos) {

        Set<Team> teams = new HashSet<>();
        for (TeamDto teamDto : teamDtos) {
            Team team = convertDtoToEntity(teamDto);
            teams.add(team);
        }
        return teams;
    }

    public Set<TeamDto> convertSetEntityToDto(Set<Team> teams) {

        Set<TeamDto> teamDtos = new HashSet<>();
        for (Team team : teams) {
            TeamDto teamDto = convertEntityToDto(team);
            teamDtos.add(teamDto);
        }
        return teamDtos;
    }
}
