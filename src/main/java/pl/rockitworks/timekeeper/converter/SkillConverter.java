package pl.rockitworks.timekeeper.converter;
//Author: Sławomir Rogala

import org.springframework.beans.factory.annotation.Autowired;
import pl.rockitworks.timekeeper.dto.SkillDto;
import pl.rockitworks.timekeeper.entity.Skill;
import pl.rockitworks.timekeeper.repository.SkillRepository;

import java.util.HashSet;
import java.util.Set;

public class SkillConverter {

    @Autowired
    SkillRepository skillRepository;

     public SkillDto convertEntityToDto (Skill skill) {
        SkillDto skillDto = new SkillDto();
        skillDto.setId(skill.getId());
        skillDto.setName(skill.getName());
        return skillDto;
    }

    public Skill convertDtoToEntity(SkillDto skillDto) {
        Skill skillDb = skillRepository.getOne(skillDto.getId());
        skillDb.setId(skillDto.getId());
        skillDb.setName(skillDto.getName());
        return skillDb;
    }

    public Set<SkillDto> convertSetEntityToDto(Set<Skill> skills) {
        Set<SkillDto> skillDtos = new HashSet<>();
        for (Skill skill : skills) {
            SkillDto skillDto = convertEntityToDto(skill);
            skillDtos.add(skillDto);
        }
        return  skillDtos;
    }

    public Set<Skill> convertSetDtoToEntity (Set<SkillDto> skillDtos) {
        Set<Skill> skills = new HashSet<>();
        for (SkillDto skillDto : skillDtos) {
            Skill skill = convertDtoToEntity(skillDto);
            skills.add(skill);
        }
        return skills;
    }
}
