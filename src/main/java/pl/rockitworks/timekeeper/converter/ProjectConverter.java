package pl.rockitworks.timekeeper.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.rockitworks.timekeeper.dto.ProjectDto;
import pl.rockitworks.timekeeper.entity.Project;
import pl.rockitworks.timekeeper.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Norbert Ciak
 */
@Component
public class ProjectConverter {

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    UserConverter userConverter;


    public ProjectDto convertEntityToDto(Project project) {
        ProjectDto projectDto = new ProjectDto();
        projectDto.setId(project.getId());
        projectDto.setName(project.getName());
        projectDto.setIsActive(project.getIsActive());
        projectDto.setStartDate(project.getStartDate());
        projectDto.setEndDate(project.getEndDate());
        projectDto.setCreatedBy(userConverter.convertEntityToDto(project.getCreatedBy()));
        return projectDto;
    }

    public Project convertDtoToEntity(ProjectDto projectDto) {
        Project project = projectRepository.getOne(projectDto.getId());
        project.setId(projectDto.getId());
        project.setName(projectDto.getName());
        project.setIsActive(projectDto.getIsActive());
        project.setStartDate(projectDto.getStartDate());
        project.setEndDate(projectDto.getEndDate());
        project.setCreatedBy(userConverter.convertDtoToEntity(projectDto.getCreatedBy()));
        return project;
    }

    public Set<ProjectDto> convertSetEntityToDto(Set<Project> projects) {
        Set<ProjectDto> projectDtos = new HashSet<>();
        for (Project project : projects) {
            ProjectDto projectDto = convertEntityToDto(project);
            projectDtos.add(projectDto);
        }
        return projectDtos;
    }

    public Set<Project> convertSetDtoToEntity(Set<ProjectDto> projectDtos) {
        Set<Project> projects = new HashSet<>();
        for (ProjectDto projectDto : projectDtos) {
            Project project = convertDtoToEntity(projectDto);
            projects.add(project);
        }
        return projects;
    }
}

