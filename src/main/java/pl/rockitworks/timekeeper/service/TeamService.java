package pl.rockitworks.timekeeper.service;

import pl.rockitworks.timekeeper.dto.TeamDto;
import pl.rockitworks.timekeeper.entity.Team;

import java.util.Set;

/**
 * @author Kamil Ulatowski
 */
public interface TeamService {

    TeamDto findByName(String name);

    Set<TeamDto> findByNamePattern(String pattern);

    Team saveTeam(TeamDto team);

    void deleteTeam(TeamDto team);
}
