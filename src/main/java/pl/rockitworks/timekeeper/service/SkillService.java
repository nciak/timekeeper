package pl.rockitworks.timekeeper.service;
//Author: Sławomir Rogala

import pl.rockitworks.timekeeper.entity.Skill;

import java.util.List;

public interface SkillService {

    Skill findBySkillName(String name);
    List<Skill> listAllSkills();
    Skill saveSkill(Skill skill);
    void deleteSkill(Skill skill);
}
