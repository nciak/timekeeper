package pl.rockitworks.timekeeper.service.impl;
//Author: Sławomir Rogala

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rockitworks.timekeeper.dto.SkillDto;
import pl.rockitworks.timekeeper.entity.Skill;
import pl.rockitworks.timekeeper.repository.SkillRepository;
import pl.rockitworks.timekeeper.service.SkillService;

import java.util.List;

@Service
public class SkillServiceImpl implements SkillService {

    private final SkillRepository skillRepository;

    @Autowired
    public SkillServiceImpl(SkillRepository skillRepository) {
        this.skillRepository = skillRepository;
    }

    @Override
    public Skill findBySkillName(String name) {
        SkillDto skillDto  = new SkillDto();
        skillDto.getId();
        return skillRepository.findOneByName(name);
    }

    @Override
    public List<Skill> listAllSkills() {
        return skillRepository.findAll();
    }

    @Override
    public Skill saveSkill(Skill skill) {
        return skillRepository.save(skill);
    }

    @Override
    public void deleteSkill(Skill skill) {
        skillRepository.delete(skill);
    }
}

