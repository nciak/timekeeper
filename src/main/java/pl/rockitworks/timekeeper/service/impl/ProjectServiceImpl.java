package pl.rockitworks.timekeeper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.rockitworks.timekeeper.converter.ProjectConverter;
import pl.rockitworks.timekeeper.dto.ProjectDto;
import pl.rockitworks.timekeeper.entity.Project;
import pl.rockitworks.timekeeper.repository.ProjectRepository;
import pl.rockitworks.timekeeper.service.ProjectService;

import java.util.Set;

/**
 * @author Norbert Ciak
 */

@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private static ProjectRepository projectRepository;

    @Autowired
    private static ProjectConverter projectConverter;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public ProjectDto saveProject(ProjectDto projectDto) {
        Project project = projectConverter.convertDtoToEntity(projectDto);
        projectRepository.save(project);
        return projectDto;
    }

    @Override
    public void deleteProject(ProjectDto projectDto) {
        Project project = projectConverter.convertDtoToEntity(projectDto);
        projectRepository.delete(project);
    }

    @Override
    public Set<ProjectDto> findByIsActive(Boolean isActive) {
        Set<Project> projects = projectRepository.findByIsActive(isActive);
        Set<ProjectDto> projectDtos = projectConverter.convertSetEntityToDto(projects);
        return projectDtos;
    }

}
