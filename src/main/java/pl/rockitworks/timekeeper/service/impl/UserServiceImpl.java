package pl.rockitworks.timekeeper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.rockitworks.timekeeper.entity.User;
import pl.rockitworks.timekeeper.repository.UserRepository;
import pl.rockitworks.timekeeper.service.UserService;


import java.util.List;
import java.util.Set;
/**
 * @author Bartosz Flis
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findUserByName(String name) {
        return userRepository.findOneByName(name);
    }

    @Override
    public Set<User> findUserBySome(String partName) {
        return userRepository.findByNameLike(partName);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public User saveUser(User user) {
        userRepository.save(user);
        return user;
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);

    }
}
