package pl.rockitworks.timekeeper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.rockitworks.timekeeper.converter.UserConverter;
import pl.rockitworks.timekeeper.dto.TeamDto;
import pl.rockitworks.timekeeper.entity.Team;
import pl.rockitworks.timekeeper.repository.TeamRepository;
import pl.rockitworks.timekeeper.service.TeamService;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Kamil Ulatowski
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
public class TeamServiceImpl implements TeamService {

    @Autowired
    private final TeamRepository teamRepository;
    private UserConverter userConverter;

    public TeamServiceImpl(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Override
    public TeamDto findByName(String name) {

        Team team = teamRepository.findByName(name);
        TeamDto teamDto = new TeamDto();
        teamDto.setId(team.getId());
        teamDto.setName(team.getName());
        teamDto.setLeader(userConverter.convertEntityToDto(team.getLeader()));
        return teamDto;
    }

    @Override
    public Set<TeamDto> findByNamePattern(String pattern) {

        Set<Team> teams = teamRepository.findByNameLike(pattern);
        Set<TeamDto> teamDtos = new HashSet<>();
        TeamDto teamDto = new TeamDto();
        for (Team t : teams) {

            teamDto.setId(t.getId());
            teamDto.setName(t.getName());
            teamDto.setLeader(userConverter.convertEntityToDto(t.getLeader()));

            teamDtos.add(teamDto);
        }
        return teamDtos;
    }

    @Override
    public Team saveTeam(TeamDto teamDto) {

        Team team = new Team();
        team.setId(teamDto.getId());
        team.setName(teamDto.getName());
        team.setLeader(userConverter.convertDtoToEntity(teamDto.getLeader()));
        return teamRepository.save(team);
    }

    @Override
    public void deleteTeam(TeamDto teamDto) {

        Team team = new Team();
        team.setId(teamDto.getId());
        team.setName(teamDto.getName());
        team.setLeader(userConverter.convertDtoToEntity(teamDto.getLeader()));
        teamRepository.delete(team);
    }

}
