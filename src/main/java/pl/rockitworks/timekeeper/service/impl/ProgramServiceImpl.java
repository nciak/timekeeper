package pl.rockitworks.timekeeper.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.rockitworks.timekeeper.converter.ProgramConverter;
import pl.rockitworks.timekeeper.dto.ProgramDto;
import pl.rockitworks.timekeeper.dto.UserDto;
import pl.rockitworks.timekeeper.entity.Program;
import pl.rockitworks.timekeeper.entity.User;
import pl.rockitworks.timekeeper.repository.ProgramRepository;
import pl.rockitworks.timekeeper.service.ProgramService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@Transactional
public class ProgramServiceImpl implements ProgramService
{
    private final ProgramRepository programRepository;
    private final ProgramConverter programConverter;

    @Autowired
    public ProgramServiceImpl(ProgramRepository programRepository, ProgramConverter programConverter) {

        this.programRepository = programRepository;
        this.programConverter = programConverter;
    }

    @Override
    public ProgramDto findProgramByName(String name)
    {
        Program programDb = programRepository.findOneByName(name);
        return programConverter.convertEntityToDto(programDb);
    }

    @Override
    public Set<ProgramDto> findProgramBySome(String some)
    {
        List<Program> programsFromDb = programRepository.findBySome(some);
        Set<Program> programSetFromDb = new HashSet<>(programsFromDb);
        Set<ProgramDto> programDtos = programConverter.convertSetFromEntityToDto(programSetFromDb);

        return programDtos;
    }

    @Override
    public ProgramDto saveProgram(ProgramDto programDto)
    {
        Program programInDb = programConverter.convertDtoToEntity(programDto);
        programRepository.save(programInDb);
        return programDto;
    }

    @Override
    public void deleteProgram(ProgramDto programDto)
    {
        Program programInDb = programConverter.convertDtoToEntity(programDto);
        programRepository.delete(programInDb);
    }
}
