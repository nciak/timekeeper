package pl.rockitworks.timekeeper.service;

import org.springframework.stereotype.Service;
import pl.rockitworks.timekeeper.dto.ProgramDto;
import pl.rockitworks.timekeeper.entity.Program;

import java.util.List;
import java.util.Set;


public interface ProgramService
{
    ProgramDto findProgramByName (String name);
    Set<ProgramDto> findProgramBySome (String some);
    ProgramDto saveProgram (ProgramDto programDto);
    void deleteProgram (ProgramDto programDto);
}
