package pl.rockitworks.timekeeper.service;

import pl.rockitworks.timekeeper.dto.UserDto;
import pl.rockitworks.timekeeper.entity.User;

import java.util.List;
import java.util.Set;

/**
 * @author Bartosz Flis
 */
public interface UserService {
    User findUserByName(String name);

    Set<User> findUserBySome(String some);

    User findUserByEmail(String email);

    User saveUser(User user);

    void deleteUser(User user);



}
