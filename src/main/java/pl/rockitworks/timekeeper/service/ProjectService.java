package pl.rockitworks.timekeeper.service;

import pl.rockitworks.timekeeper.dto.ProjectDto;
import pl.rockitworks.timekeeper.entity.Project;

import java.util.List;
import java.util.Set;

/**
 * @author Norbert Ciak
 */

public interface ProjectService {

    ProjectDto saveProject(ProjectDto projectDto);
    void deleteProject(ProjectDto projectDto);
    Set<ProjectDto> findByIsActive(Boolean isActive);


}
