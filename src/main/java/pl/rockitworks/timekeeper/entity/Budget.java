package pl.rockitworks.timekeeper.entity;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
 * @author Norbert Ciak
 */
@Entity
@Table(name = "budget")
@Getter
@Setter
public class Budget {

    @Id
    @GeneratedValue
    private Long id;

    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;

    @Column(name = "revenue")
    private BigDecimal revenue;


}
