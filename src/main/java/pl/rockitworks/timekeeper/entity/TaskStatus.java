package pl.rockitworks.timekeeper.entity;

public enum TaskStatus {
    IN_PROGRESS,
    TESTS,
    REJECTED,
    CHANGE,
    DONE;
}
