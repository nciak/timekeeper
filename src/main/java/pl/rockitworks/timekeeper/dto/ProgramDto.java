package pl.rockitworks.timekeeper.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.rockitworks.timekeeper.entity.User;

import java.util.Set;
//Jakub Grodzinski

@Getter
@Setter
public class ProgramDto
{

    private Long id;

    private String name;

    private UserDto createdBy;

    private Set<ProjectDto> projectDtos;



}
