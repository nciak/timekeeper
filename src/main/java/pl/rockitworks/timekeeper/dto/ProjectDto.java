package pl.rockitworks.timekeeper.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import pl.rockitworks.timekeeper.entity.Budget;
import pl.rockitworks.timekeeper.entity.Task;
import pl.rockitworks.timekeeper.entity.Team;
import pl.rockitworks.timekeeper.entity.User;

import java.util.Date;
import java.util.Set;

/**
 * @author Norbert Ciak
 */
@Getter
@Setter
public class ProjectDto {

    private Long id;

    private String name;

//    private ProgramDto program;

//    private BudgetDto budget;

    private UserDto createdBy;

    private Date startDate;

    private Date endDate;

    private Boolean isActive;

//    private TeamDto team;

//    private Set<TaskDto> tasks;
}
