package pl.rockitworks.timekeeper.dto;

import lombok.Getter;
import lombok.Setter;
import pl.rockitworks.timekeeper.entity.User;

/**
 * @author Bartosz Flis
 */
@Getter
@Setter
public class UserDto {

    private Long id;

    private String name;

    private String email;



}
