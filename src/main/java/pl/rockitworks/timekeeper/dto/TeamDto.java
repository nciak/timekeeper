package pl.rockitworks.timekeeper.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

// @Kamil Ulatowski
@Getter
@Setter
@ToString
public class TeamDto {

    private Long id;

    private String name;

    private UserDto leader;

    private Set<ProjectDto> projects;

    private Set<UserDto> members;
}
