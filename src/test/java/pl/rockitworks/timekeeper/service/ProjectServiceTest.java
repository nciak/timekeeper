package pl.rockitworks.timekeeper.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import pl.rockitworks.timekeeper.converter.ProjectConverter;
import pl.rockitworks.timekeeper.dto.ProjectDto;
import pl.rockitworks.timekeeper.entity.Project;
import pl.rockitworks.timekeeper.repository.ProjectRepository;
import pl.rockitworks.timekeeper.service.impl.ProjectServiceImpl;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.ArgumentMatchers.any;

/**
 * @author Norbert Ciak
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class ProjectServiceTest {

    @InjectMocks
    private ProjectServiceImpl projectService;

    @Mock
    private ProjectRepository projectRepository;


    @Test
    public void when_save_project_then_return_true() {

        Project project = new Project();
        ProjectDto projectDto = new ProjectDto();
        when(projectRepository.save(any(Project.class))).thenReturn(project);

        ProjectDto result = projectService.saveProject(projectDto);

        assertThat(result).isEqualTo(project);
    }

    @Test
    public void when_delete_project_then_return_true() {
        ProjectDto projectDto = new ProjectDto();

        projectService.saveProject(projectDto);
        projectService.deleteProject(projectDto);

        assertThat(projectRepository.findAll()).isEmpty();
    }

    @Test
    public void when_searching_by_active_then_return_objects() {
        Project project = new Project();
        project.setIsActive(true);
        Set<Project> projects = new HashSet<>();
        projects.add(project);
        projectRepository.saveAll(projects);
        when(projectRepository.findByIsActive(true)).thenReturn(projects);

        Set<ProjectDto> result = projectService.findByIsActive(true);

        assertThat(projects).isEqualTo(result);
    }

}
