package pl.rockitworks.timekeeper.service;
//Author: Sławomir Rogala

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.rockitworks.timekeeper.entity.Skill;
import pl.rockitworks.timekeeper.repository.SkillRepository;
import pl.rockitworks.timekeeper.service.impl.SkillServiceImpl;


import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class SkillServiceTest {

    @InjectMocks
    private SkillServiceImpl skillService;

    @Mock
    private SkillRepository skillRepository;

    @Before
    public void setUp() {
        skillRepository = mock(SkillRepository.class);
        skillService = new SkillServiceImpl(skillRepository);
    }

    @Test
    public void test_find() {
        Skill skill = new Skill();
        skill.setName("skill");
        when(skillRepository.findOneByName("skill")).thenReturn(skill);

        Skill result = skillService.findBySkillName("skill");

        assertThat(result).isEqualTo(skill);
    }

    @Test
    public void when_save_skill_then_return_true() {

        Skill skill = new Skill();
        when(skillRepository.save(skill)).thenReturn(skill);

        Skill result = skillService.saveSkill(skill);

        assertThat(result).isEqualTo(skill);
    }

    @Test
    public void when_delete_skill_then_return_true() {

        Skill skill = new Skill();
        when(skillRepository.save(skill)).thenReturn(skill);

        skillService.deleteSkill(skill);

        assertThat(skillRepository.findAll()).isEmpty();
    }

}
