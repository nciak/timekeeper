package pl.rockitworks.timekeeper.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.rockitworks.timekeeper.dto.TeamDto;
import pl.rockitworks.timekeeper.entity.Team;
import pl.rockitworks.timekeeper.repository.TeamRepository;
import pl.rockitworks.timekeeper.service.impl.TeamServiceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * @author Kamil Ulatowski
 */
@RunWith(MockitoJUnitRunner.class)
public class TeamServiceTest {

    @InjectMocks
    private TeamServiceImpl teamServiceImpl;

    @Mock
    private TeamRepository teamRepository;

    @Test
    public void testFindTeamByName() {

        Team teamA = new Team();
        teamA.setName("Team A");
        when(teamRepository.findByName("Team A")).thenReturn(teamA);

        TeamDto result = teamServiceImpl.findByName("Team A");

        assertThat(result.getName()).isEqualTo(teamA.getName());
    }

    @Test
    public void testFindNameByPattern() {

        Team teamA = new Team();
        Team teamB = new Team();
        teamA.setName("Gdansk");
        teamB.setName("Gdynia");

        Set<Team> teams = new HashSet<>();
        teams.add(teamA);
        teams.add(teamB);
        when(teamRepository.findByNameLike("Gd")).thenReturn(teams);

        Set<TeamDto> result = teamServiceImpl.findByNamePattern("Gd");

        assertThat(teams).isNotEqualTo(result);
    }

    @Test
    public void testSaveTeam() {

        Team teamA = new Team();
        TeamDto teamDto = new TeamDto();
        teamA.setName("Team A");
        teamA.setName("Team A");
        when(teamRepository.save(any(Team.class))).thenReturn(teamA);

        Team result = teamServiceImpl.saveTeam(teamDto);

        assertThat(result).isNotNull();
        assertThat(result).isEqualTo(teamA);
    }

    @Test
    public void testDeleteTeam() {

        Team teamA = new Team();
        TeamDto teamDto = new TeamDto();
        teamA.setName("McDonald");
        teamDto.setName("McDonald");
        teamRepository.save(teamA);

        teamServiceImpl.deleteTeam(teamDto);

        assertThat(teamRepository.findAll()).isEmpty();
    }
}
