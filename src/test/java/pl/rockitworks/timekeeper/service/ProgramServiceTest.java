package pl.rockitworks.timekeeper.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;

import org.mockito.stubbing.Answer;
import pl.rockitworks.timekeeper.dto.ProgramDto;
import pl.rockitworks.timekeeper.entity.Program;
import pl.rockitworks.timekeeper.repository.ProgramRepository;
import pl.rockitworks.timekeeper.service.impl.ProgramServiceImpl;

import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.when;
import static org.assertj.core.api.Assertions.assertThat;
// Jakub Grodzinski
@RunWith(MockitoJUnitRunner.class)
public class ProgramServiceTest
{
    @InjectMocks
    private ProgramServiceImpl programService;

    @Mock
    private ProgramRepository programRepository;

//    @Before
//    public void constructor ()
//    {
//        programService = new ProgramServiceImpl(programRepository);
//    }

    @Test

    public void testFindInDb() {

        Program program = new Program();
        program.setName("program");
        when(programRepository.findOneByName("program")).thenReturn(program);

        ProgramDto result = programService.findProgramByName("program");

        assertThat(result).isEqualTo(program);
    }

    @Test
    public void testSaveToDb ()
    {
        Program program = new Program();
        ProgramDto programDto = new ProgramDto();
        program.setName("program");
        programDto.setName("program");
        when(programRepository.save(any(Program.class))).thenReturn(program);

        ProgramDto result = programService.saveProgram(programDto);

        assertThat(result.getName()).isEqualTo(program.getName());
    }

    /*@Test
    public void testDeleteFromDb ()
    {

        Program program = new Program();
        ProgramDto programDto = new ProgramDto();
        program.setId(1L);
        programDto.setId(1L);
        program.setName("program");
        programDto.setName("program");
        doAnswer((i) -> {
            assertThat(program).isEqualTo(i.getArgument(0));
            return null;
        }).when(programRepository).delete(program);
        programService.deleteProgram(programDto);

    }*/



}
