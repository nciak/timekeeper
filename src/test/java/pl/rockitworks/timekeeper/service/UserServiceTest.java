package pl.rockitworks.timekeeper.service;



import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.rockitworks.timekeeper.entity.User;
import pl.rockitworks.timekeeper.repository.UserRepository;
import pl.rockitworks.timekeeper.service.impl.UserServiceImpl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;

/**
 * @author Bartosz Flis
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;

//    @Before
//    public void setUp() {
//        userRepository = mock(UserRepository.class);
//        userService = new UserServiceImpl(userRepository);
//    }

    @Test
    public void findUserByName() {
        User john = new User();
        john.setName("John");
        when(userRepository.findOneByName("John")).thenReturn(john);
        User user = userService.findUserByName("John");
        assertEquals("John", user.getName());

    }

    @Test
    public void findUserByEmail() {
        User user = new User();
        user.setEmail("john@john.com");
        when(userRepository.findByEmail("john@john.com")).thenReturn(user);
        User user1 = userService.findUserByEmail("john@john.com");
        assertEquals("john@john.com", user1.getEmail());

    }


    @Test
    public void testSaveUser() {
        User user = new User();
        user.setName("Ala");
        when(userRepository.save(user)).thenReturn(user);
        User result = userService.saveUser(user);
        assertNotNull(result);
        assertEquals(user.getName(), result.getName());

    }


    @Test
    public void testDeleteUser() {
        User user = new User();
        doAnswer((i) -> {
            assertThat(user).isEqualTo(i.getArgument(0));
            return null;
        }).when(userRepository).delete(user);
        userService.deleteUser(user);


    }

    @Test
    public void findUserBySome() {
        Set<User> users = new HashSet<>();
        User user = new User();
        user.setName("Alibaba");
        User user1 = new User();
        user1.setName("Alibaba");
        users.add(user);
        users.add(user1);
        when(userRepository.findByNameLike("Ali")).thenReturn(users);
        Set users1 = userService.findUserBySome("Ali");
        assertThat(users).isEqualTo(users1);


    }


}
