package pl.rockitworks.timekeeper.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Program;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

// @Kamil Ulatowski
@RunWith(SpringRunner.class)
@DataJpaTest
public class ProgramRepositoryTest
{

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProgramRepository programRepository;

    @Test
    public void findProgramByName() {

        Program studyCourse = new Program();
        studyCourse.setName("Study Course");
        entityManager.persist(studyCourse);

        Program result = programRepository.findOneByName("Study Course");

        assertThat(result.getName()).isEqualTo(studyCourse.getName());
    }

    @Test
    public void notNullWhenProgramFound() {

        Program windows = new Program();
        windows.setName("Windows");
        entityManager.persist(windows);

        Program result = programRepository.findOneByName("Windows");

        assertThat(result).isNotNull();
    }

    @Test
    public void returnPrograms() {

        Program win = entityManager.persistAndFlush(new Program());
        Program windows = entityManager.persistAndFlush(new Program());
        win.setName("win");
        windows.setName("windows");

        List<Program> result = programRepository.findBySome("win");

        assertThat(result).containsExactly(win, windows);
    }
}
