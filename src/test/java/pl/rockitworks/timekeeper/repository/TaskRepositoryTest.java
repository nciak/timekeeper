package pl.rockitworks.timekeeper.repository;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Task;


import static org.junit.Assert.assertTrue;

import static org.assertj.core.api.Assertions.assertThat;



import java.util.List;
// Jakub Grodzinski
@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepositoryTest
{
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void findByName ()
    {
        Task task = new Task();
        task.setName("Inwestycja");
        testEntityManager.persist(task);

        List<Task> result = taskRepository.findAllByName("Inwestycja");
        assertTrue(result.contains(task));
    }

    @Test
    public void findTwoByName ()
    {
        Task task = new Task();
        Task task1 = new Task();

        task.setName("program");
        task1.setName("projekt");


        testEntityManager.persist(task);
        testEntityManager.persist(task1);

        List<Task> result = taskRepository.findAlike("pro");
        assertThat(result).containsExactlyInAnyOrder(task, task1);
    }

    @Test
    public void deleteFromMemory ()
    {
        Task task = new Task();
        Task task1 = new Task();
        task.setName("lol");
        task1.setName("olo");
        testEntityManager.persist(task);
        testEntityManager.persist(task1);

        taskRepository.deleteByName("lol");

        assertThat(taskRepository.findAll()).containsOnly(task1);
    }


}
