package pl.rockitworks.timekeeper.repository;
//Author: Sławomir Rogala

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Skill;

import pl.rockitworks.timekeeper.entity.Task;


import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SkillRepositoryTest {

    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private SkillRepository skillRepository;

    @Test
    public void findAllByName() {

        Skill skill = new Skill();
        skill.setName("Tester");
        testEntityManager.persist(skill);

        Skill result = skillRepository.findOneByName("Tester");
        assertEquals(result.getName(), skill.getName());
    }
}
