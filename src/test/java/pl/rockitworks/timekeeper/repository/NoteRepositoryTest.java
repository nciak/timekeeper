package pl.rockitworks.timekeeper.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Note;
import pl.rockitworks.timekeeper.entity.Task;
import pl.rockitworks.timekeeper.entity.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
//Jakub Grodzinski
@RunWith(SpringRunner.class)
@DataJpaTest
public class NoteRepositoryTest
{
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private NoteRepository noteRepository;

    @Test
    public void testSearchForNotes ()
    {
        Note note = new Note();
        User user = new User();
        Task task = new Task();
        Date date = new Date();
        note.setComment("lalala");
        note.setCreatedDate(date);

        testEntityManager.persist(user);
        testEntityManager.persist(task);

        note.setUser(user);
        note.setTask(task);

        testEntityManager.persist(note);

        List<Note> notes = noteRepository.selectPassingNotes("lala");

        assertThat(notes).contains(note);
    }

    @Test
    public void testDeleteNote ()
    {
        Note note = new Note();
        User user = new User();
        Task task = new Task();
        Date date = new Date();
        note.setComment("lalala");
        note.setCreatedDate(date);

        testEntityManager.persist(user);
        testEntityManager.persist(task);

        note.setUser(user);
        note.setTask(task);

        testEntityManager.persist(note);

        noteRepository.delete(note);

        assertThat(noteRepository.findAll()).isEmpty();
    }
}
