package pl.rockitworks.timekeeper.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.rockitworks.timekeeper.entity.Budget;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Norbert Ciak
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class BudgetRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BudgetRepository budgetRepository;

    @Test
    public void create() {

        Budget budget = new Budget();
        Budget budget2 = new Budget();

        List<Budget> budgets = new ArrayList<>();
        budgets.add(budget);
        budgets.add(budget2);

        entityManager.persist(budget);
        entityManager.persist(budget2);
        budgetRepository.saveAll(budgets);

        assertThat(budgetRepository.count()).isNotZero();

    }

    @Test
    public void findByRevenue() {
        Budget budget = new Budget();
        budget.setRevenue(new BigDecimal("23000.44"));
        entityManager.persist(budget);

        Budget result = budgetRepository.findByRevenue(new BigDecimal("23000.44"));

        assertThat(result.getRevenue()).isEqualTo(budget.getRevenue());

    }

    @Test
    public void delete() {
        Budget budget = new Budget();
        entityManager.persist(budget);

        budgetRepository.delete(budget);

        assertThat(budgetRepository.findAll().isEmpty());
    }

}

