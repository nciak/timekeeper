package pl.rockitworks.timekeeper.repository;

//Author: Norbert Ciak

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import pl.rockitworks.timekeeper.entity.Project;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Norbert Ciak
 */

@RunWith(SpringRunner.class)
@DataJpaTest
@Transactional
public class ProjectRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private ProjectRepository projectRepository;

    @Test
    public void create() {

        Project project = new Project();
        Project project2 = new Project();

        List<Project> projects = new ArrayList<>();
        projects.add(project);
        projects.add(project2);

        entityManager.persist(project);
        entityManager.persist(project2);
        projectRepository.saveAll(projects);

        assertThat(projectRepository.count()).isNotZero();

    }

    @Test
    public void findByName() {
        Project project = new Project();
        project.setName("Test Name");
        entityManager.persist(project);

        Project result = projectRepository.findByName("Test Name");

        assertThat(result.getName()).isEqualTo(project.getName());

    }

    @Test
    public void delete() {
        Project project = new Project();
        entityManager.persist(project);

        projectRepository.delete(project);

        assertThat(projectRepository.findAll().isEmpty());
    }

}
