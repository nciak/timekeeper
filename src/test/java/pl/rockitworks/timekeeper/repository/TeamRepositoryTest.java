package pl.rockitworks.timekeeper.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Team;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Kamil Ulatowski
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TeamRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TeamRepository teamRepository;

    @Test
    public void findTeamByName() {

        Team teamA = new Team();
        teamA.setName("Team A");
        entityManager.persist(teamA);

        Team result = teamRepository.findByName("Team A");

        assertThat(result.getName()).isEqualTo(teamA.getName());
    }

    @Test
    public void notNullWhenTeamFound() {

        Team boys = new Team();
        boys.setName("Boys Band");
        entityManager.persist(boys);

        Team result = teamRepository.findByName("Boys Band");

        assertThat(result).isNotNull();
    }

    @Test
    public void returnPrograms() {

        Team boys = entityManager.persistAndFlush(new Team());
        Team boysBand = entityManager.persistAndFlush(new Team());
        boys.setName("Boys");
        boysBand.setName("Boys Band");

        Set<Team> result = teamRepository.findByNameLike("Boy");

        assertThat(boysBand.getName()).startsWith(boys.getName());
    }

}
