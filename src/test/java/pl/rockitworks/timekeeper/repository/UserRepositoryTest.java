package pl.rockitworks.timekeeper.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.User;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

// @Kamil Ulatowski
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findUserByName() {

        User john = new User();
        john.setName("John");
        entityManager.persist(john);

        User result = userRepository.findOneByName("John");

        assertThat(result.getName()).isEqualTo(john.getName());
    }

    @Test
    public void notNullWhenUserFound() {

        User mark = new User();
        mark.setName("Mark");
        entityManager.persist(mark);

        User result = userRepository.findOneByName("John");

        assertThat(result).isNull();
    }

    @Test
    public void returnUsers() {

        User jo = entityManager.persistAndFlush(new User());
        User john = entityManager.persistAndFlush(new User());
        jo.setName("Jo");
        john.setName("John");

        Set<User> result = userRepository.findByNameLike("Jo");

        assertThat(result).containsExactly(jo, john);
    }
}
