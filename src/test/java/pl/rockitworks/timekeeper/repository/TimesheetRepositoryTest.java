package pl.rockitworks.timekeeper.repository;
//Author: Sławomir Rogala

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;
import pl.rockitworks.timekeeper.entity.Timesheet;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TimesheetRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private TimesheetRepository timesheetRepository;

    @Test
    public void findByValue(){
        //given
        Timesheet timesheet = new Timesheet();
        timesheet.setValue(1);
        entityManager.persist(timesheet);
        //when
        Timesheet result = timesheetRepository.findByValue(1);
        //then
        assertThat(result.getValue()).isEqualTo(timesheet.getValue());
    }
}
